def adj_num_ord(num) : 
    if not isinstance(num, int) and not isinstance(num, str) : 
        raise TypeError("Le paramètre d'entrer doit être un entier ou une string")
    if int(num) < 0 :
        raise ValueError("Le paramètre doit être un entier naturel strictement positif")
    else : 
        if int(num) == 1 :
            return "1ère"
        else : 
            return str(num)+ "ème"


def where_he_is() : 

    txt = input("Entrer une phrase : ").lower()
    car = input("Entrer un caractère à rechercher : ").lower()
    i = 0

    for pos, char in enumerate(txt) :
        if char == car :
            i += 1  
            print( "On retrouve le caractère " + char + " à la "+ str(adj_num_ord(pos)) +" position ")

    if i == 0 :
        return print("Le caractère n'est pas présent dans la phrase.") 

where_he_is()